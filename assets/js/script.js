$(document).ready(function () {

    /* =========================  form validation for comments begin ================================  */

    // function to validate Comment Description

    function validateCommentDescription() {

        var commentDescription = $("#comment-description").val();

        if(commentDescription == "") {

            $("#comment-description").css({
                "border":"1px solid red",
            });

            $("#comment-description-error").html("Please add a comment").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#comment-description").css({
                "border":"1px solid green",
            });

            $("#comment-description-error").empty();
        }

        return false;
    }

    // setting up behavior for the blur function
    $("#comment-description").blur(function () {
        validateCommentDescription();
    })

    $("#comment-description").focus(function () {
        $("#comment-description").css({
            "border":"0px",
            "background":"#fff",
        });
    })

    //submitting the form of comments.
    $("#comment-form").submit(function () {
        if(validateCommentDescription()) {
            event.preventDefault();
        } else {
            alert("comment added successfully");
        }
    });

    /* =========================  form validation for comments Ends ================================  */

    /* =========================  form validation for edit comments begin ==========================  */

    // function to validate Comment Description

    function validateCommentDescriptionEdit() {

        var commentEditDescription = $("#comment-edit-description").val();

        if(commentEditDescription == "") {

            $("#comment-edit-description").css({
                "border":"1px solid red",
            });

            $("#comment-edit-description-error").html("Please add a comment").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#comment-edit-description").css({
                "border":"1px solid green",
            });

            $("#comment-edit-description-error").empty();
        }

        return false;
    }

    // setting up behavior for the blur function
    $("#comment-edit-description").blur(function () {
        validateCommentDescriptionEdit();
    })

    $("#comment-edit-description").focus(function () {
        $("#comment-edit-description").css({
            "border":"0px",
            "background":"#fff",
        });
    })


    $("#comment-edit-form").submit(function () {
        if(validateCommentDescriptionEdit()) {
            event.preventDefault();
        } else {
            alert("comment editted successfully");
        }
    });

    /* =========================  form validation for edit comments end ==========================  */

$(document).ready(function () {

    /* =========================  form validation for comments begin ================================  */

    // function to validate Comment Description

    function validateCommentDescription() {

        var commentDescription = $("#comment-description").val();

        if(commentDescription == "") {

            $("#comment-description").css({
                "border":"1px solid red",
            });

            $("#comment-description-error").html("Please add a comment").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#comment-description").css({
                "border":"1px solid green",
            });

            $("#comment-description-error").empty();
        }

        return false;
    }

    // setting up behavior for the blur function
    $("#comment-description").blur(function () {
        validateCommentDescription();
    })

    $("#comment-description").focus(function () {
        $("#comment-description").css({
            "border":"0px",
            "background":"#fff",
        });
    })

    //submitting the form of comments.
    $("#comment-form").submit(function () {
        if(validateCommentDescription()) {
            event.preventDefault();
        } else {
            alert("comment added successfully");
        }
    });

    /* =========================  form validation for comments Ends ================================  */

    /* =========================  form validation for edit comments begin ==========================  */

    // function to validate Comment Description

    function validateCommentDescriptionEdit() {

        var commentEditDescription = $("#comment-edit-description").val();

        if(commentEditDescription == "") {

            $("#comment-edit-description").css({
                "border":"1px solid red",
            });

            $("#comment-edit-description-error").html("Please add a comment").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#comment-edit-description").css({
                "border":"1px solid green",
            });

            $("#comment-edit-description-error").empty();
        }

        return false;
    }

    // setting up behavior for the blur function
    $("#comment-edit-description").blur(function () {
        validateCommentDescriptionEdit();
    })

    $("#comment-edit-description").focus(function () {
        $("#comment-edit-description").css({
            "border":"0px",
            "background":"#fff",
        });
    })


    $("#comment-edit-form").submit(function () {
        if(validateCommentDescriptionEdit()) {
            event.preventDefault();
        } else {
            alert("comment editted successfully");
        }
    });

    /* =========================  form validation for edit comments end ==========================  */

    /* =========================  form validation for post Begin =================================  */

    $("#post-title").blur(function () {
        validatePostTitle();
    })

    $("#post-title").focus(function () {
        $("#post-title").css({
            "border":"0px",
            "background":"#fff",
        });
    })

    $("#post-user").blur(function () {
        validatePostUser();

    })


    $("#post-user").focus(function () {
        $("#post-user").css({
            "border":"0px",
            "background":"#fff",
        });
    })


    $("#post-descriptioon").blur(function () {
        validatePostDescription();
    })

    $("#post-descriptioon").focus(function () {
        $("#post-descriptioon").css({
            "border":"0px",
            "background":"#fff",
        });
    })

    function validatePostTitle() {

        var postTitle = $("#post-title").val();

        if(postTitle == "") {

            $("#post-title").css({
                "border":"1px solid red",
            });

            $("#post-title-error").html("invalid Post Title").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#post-title").css({
                "border":"1px solid green",
            });

            $("#post-title-error").empty();
        }

        return false;
    }

    function validatePostUser() {

        var postUser = $("#post-user").val();

        if(postUser == "") {

            $("#post-user").css({
                "border":"1px solid red",
            });

            $("#post-user-error").html("please select a user").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#post-user").css({
                "border":"1px solid green",
            });

            $("#post-user-error").empty();
        }

        return false;
    }




    function validatePostDescription() {

        var postDescription = $("#post-descriptioon").val();

        if(postDescription == "") {

            $("#post-descriptioon").css({
                "border":"1px solid red",
            });

            $("#post-description-error").html("invalid description").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#post-descriptioon").css({
                "border":"1px solid green",
            });

            $("#post-description-error").empty();
        }

        return false;
    }

    $("#post-form").submit(function () {
        if(validatePostTitle() || validatePostDescription() || validatePostUser() ) {
            validatePostTitle();
            validatePostUser();
            validatePostDescription();
            return false;

        } else {

            alert("Operation Done successfully");
            return true;
        }
    });

    //+++++++++++++++++++++++++ User Form Validation+++++++++++++++++++++++++++++++

    $("#first-name").blur(function () {
        validateFirstName();
    })

    $("#first-name").focus(function () {
        $("#first-name").css({
            "border":"0px",
            "background":"#fff",
        });
    })

    $("#email").blur(function () {
        validateEmail();
    })

    $("#email").focus(function () {
        $("#email").css({
            "border":"0px",
            "background":"#fff",
        });
    })



    // function to validate first_name

    function validateFirstName() {

        var firstName = $("#first-name").val();

        if(firstName == "" ||  (!/^[a-zA-Z]*$/g.test(firstName))) { // test ??

            $("#first-name").css({
                "border":"1px solid red",
            });

            $("#first-name-error").html("invalid first Name").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#first-name").css({
                "border":"1px solid green",
            });

            $("#first-name-error").empty();
        }

        return false;
    }

    // validating email

    function validateEmail() {

        var email = $("#email").val();
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(email == "" || (!mailformat.test(email))) { // test ??

            $("#email").css({
                "border":"1px solid red",
            });

            $("#email-error").html("invalid email address").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;

        } else {

            $("#email").css({
                "border":"1px solid green",
            });

            $("#email-error").empty();
        }

        return false;
    }

    $("#user-form").submit(function () {
        if(validateFirstName() || validateEmail() ) {
            validateFirstName();
            validateEmail();
            return false;

        } else {

            alert("user added successfully");
            return true;
        }
    });

    //++++++++++++ Update user+++++++++++++

    $("#update-user").click (function () {

        var userName = $("#user-name").val();
        var userEmail = $("#user-email").val();

        if (userName == "" || userEmail == "")
        {
            alert("Please update user not delete it.");


            /*if (userName == "")
            {
                $("#user-name").css({
                    "border":"1px solid red",
                });
                return false;
            }else
            {
                $("#user-name").css({
                    "border":"1px solid green",
                });
                return true;
            }
            if (userEmail == "")
            {
                $("#user-email").css({
                    "border":"1px solid red",
                });
                return false;
            }else
            {
                $("#user-email").css({
                    "border":"1px solid green",
                });
                return true;
            }*/
        }else
        {
            alert("Updated successfully.")
        }

    });

})

    $('.js-rating').thumbs();
// get likes
    $('.js-rating').thumbs('getLikes');
// destroy
    $('.js-rating').thumbs('destroy');
});