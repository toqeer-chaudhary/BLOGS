$(document).ready(function () {
    function like_add(post_id) {
        $.post('../pages/like_add.php', {post_id : post_id},
            function(data) {
                if (data == 'success'){
                    like_get(post_id);
                }
                else {
                    alert(data);
                }
            });
    }
    function like_get(post_id) {
        $.post('../pages/like_get.php', {post_id : post_id},
            function(data) {
                $('#post_'+ post_id+'_likes').text(data);
            });
    }
});