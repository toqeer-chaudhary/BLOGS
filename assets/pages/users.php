<?php
    include "../includes/header.php";
    include "BlogUser.php";

?>
    <!-- navbar for posts page begin -->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">BLOG</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- navbar for posts page end -->

    <!--body starts from here-->
<html>
    <body>

        <div class="bg">
            <div class="user-data">
                <form>
                    <div class="row">
                        <!--<div class="col-md-2"></div>-->
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <tr>
                                    <th>ID</th>
                                    <th>user_name</th>
                                    <th>user_email</th>
                                    <th colspan="2">Operations</th>
                                </tr>

                                <?php
                                    $user= new User();
                                    $myrows=$user->display_user();

                                        if($myrows!=0) {
                                             foreach ($myrows as $row) {
                                                 ?>
                                                 <tr>
                                                <td><?php echo $row["user_id"]; ?></td>
                                                <td><?php echo $row["user_name"]; ?></td>
                                                <td><?php echo $row['user_email']; ?></td>
                                                <td><a href="updateUser.php?update=1&rn=<?php echo $row["user_id"]; ?>& fn=<?php echo $row["user_name"]; ?>& email=<?php echo $row["user_email"]; ?>" class="btn btn-info" name="edit">Edit</a></td>

                                                <td><a href='users.php?delete=1&rn=<?php echo $row["user_id"]; ?>' onclick='return checkdelete()' class="btn btn-danger" name="delete">Delete</a></td>
                                                </tr>
                                                 <?php
                                                 }
                                                 }
                                                 else{
                                                echo"no recorde yet!";
                                                }
                                                 ?>
                             </table>
                        </div>

                    </div>
                </form>
            </div>
        </div>

    </body>
</html>


    <!--body ends here-->

<?php
    include "../includes/footer.php";
?>

<script>

    function checkdelete()
    {
        return confirm("Are you sure you want to delete this record?");
    }
</script>