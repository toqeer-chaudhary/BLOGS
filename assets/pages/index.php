<?php

        include "../includes/header.php";
        include "../includes/navbar.php";
        include "../includes/carousel.php";
 //      include "../pages/dbContext.php";
        include "BlogUser.php";
        include "BlogPost.php";
        $postObject = new BlogPost();



?>

<!-- Modal for users-->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">User Registeration</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="BlogUser.php" method="post" id="user-form">
                    <div class="row">
                        <div class="form-group col-sm-6">
<!--                            <input type="hidden" name="user_id" value="">-->
                            <label class="text-inverse" for="first_name">First Name</label>
                            <input type="text" class="form-control" id="first-name" placeholder="John" name="first_name">
                            <p id="first-name-error"></p>
                        </div>
                        <div class="form-group col-sm-6 ml-auto">
                            <label class="text-inverse" for="email">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="john@xyz.com" name="email">
                            <p id="email-error"></p>
                        </div>
                        <div class="modal-footer col-sm-12">
                            <button type="submit" class="btn btn-outline-dark" id="submit-user" name="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal for users end-->

<!-- Modal for Posts begin-->

<div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create A Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="post-form">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="text-inverse" for="post_title">Post Title</label>
                            <input type="text" class="form-control" id="post-title" placeholder="A Great World" name="post_title">
                            <p id="post-title-error"></p>
                        </div>
                        <div class="form-group col-sm-6 ml-auto">
                            <label for="post_user">Select User:</label>
                            <select class="form-control" id="post-user" name="post_user">
                                <option value="">Please Select User</option>
                                <p id="post-user-error"></p>

                                <!-- to display users in the drop down -->

                                <?php
                                $user= new User();
                                $myrows=$user->display_user();

                                if($myrows!=0)
                                {

                                foreach ($myrows as $row)
                                {

                                ?>
                                    <option value="<?php echo $row["user_name"]; ?>"><?php echo $row["user_name"]; ?></option>
                                    <?php
                                }

                                }
                                else
                                {
                                    echo"no users yet!";
                                }
                                ?>

                            </select>
                            <p id="post-user-error"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-inverse" for="post_descriptioon">POST Description</label>
                        <textarea class="form-control" rows="5" id="post-descriptioon" name="post_description" placeholder="Description"></textarea>
                        <p id="post-description-error"></p>
                    </div>
                    <div class="modal-footer col-sm-12">
                        <button type="submit" class="btn btn-outline-dark" id="create-post" name="create_post">Create Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Posts end-->

<!-- Blog Posts Begin -->
<div class="container mt-5">
    <h2 class="my-4 text-center bg-dark text-light">BLOGS Are Awesome</h2>
    <div class="row">
        <div class="col-sm-8">
            <!-- Blog Post -->
            <?php  include "showPost.php" ?>

            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                    <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">Newer &rarr;</a>
                </li>
            </ul>
        </div>

        <!-- side widget column -->
        <div class="col-sm-4">
            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-outline-dark" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">BootStrap</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">PHP</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">HTML</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Side Widget</h5>
                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, molestiae neque. Eveniet iusto magnam minima, mollitia odio repellendus. Assumenda beatae consequuntur corporis cum doloribus in obcaecati, officiis repudiandae vel voluptates.
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Blog Posts End -->


<?php  include "../includes/footer.php"; ?>











































