<?php

  //  include "dbContext.php";
   // include "BlogUser.php";

class BlogPost
{
    private $sql;
    private $postId;
    private $postTitle;
    private $postByUser;
    private $postDescription;


    public function __construct()
    {
        $this->db = new DbContext();
    }

    //Setters


    public function setPostId($postId)
    {
        $this->postId = $postId;
    }

    public function setPostTitle($postTitle)
    {
        $this->postTitle = $postTitle;
    }

    public function setPostByUser($postByUser)
    {
        $this->postByUser = $postByUser;
    }

    public function setPostDescription($postDescription)
    {
        $this->postDescription = $postDescription;
    }


    //Getters

    public function getPostId()
    {
        return $this->postId;
    }


    public function getPostTitle()
    {
        return $this->postTitle;
    }


    public function getPostByUser()
    {
        return $this->postByUser;
    }


    public function getPostDescription()
    {
        return $this->postDescription;
    }

    //Create user



    public function fetchPosts()
    {
        $this->sql = "SELECT * FROM `posts`";
        $check = $this->db->select($this->sql);

        if ($check != 0)
        {
            return $check;
        }
        else
        {
            return $check;
        }
    }

    public function fetchParticularPost()
    {
         $this->sql = "SELECT * FROM `posts` WHERE post_id = '".$this->getPostId()."'";
         $check = $this->db->select($this->sql);

        if ($check != 0)
        {
            return $check;
        }
        else
        {
            return "";
        }

    }


    public function createPost()
    {
        $db = new DbContext();
        //$db->open_connection();

        $title = $this->getPostTitle();
        $user = $this->getPostByUser();
        $description = $this->getPostDescription();

        $query = "INSERT INTO `posts`(`post_title`, `post_by_user`, `post_description`) VALUES ('$title','$user','$description')";

        $db->insert($query);
    }

    public function editPost()
    {
        $this->sql = "UPDATE `posts` SET `post_title` = '".$this->getPostTitle()."',`post_by_user` = '".$this->getPostByUser()."',`post_description` = '".$this->getPostDescription()."' WHERE `post_id` = '".$this->getPostId()."'";
        $this->db->update($this->sql);
    }

    public function deletePost()
    {
        // echo $this->comment_id;
        $this->sql = "DELETE FROM `posts` WHERE post_id = '".$this->getPostId()."'";
        $this->db->delete($this->sql);
    }
}

$post = new BlogPost();

if (isset($_POST[('create_post')]))
{

    $post->setPostTitle($_POST[('post_title')]);
    $post->setPostByUser($_POST[('post_user')]);
    $post->setPostDescription($_POST[('post_description')]);
    $post->createPost();

}

if (isset($_POST[('update_post')]))
{

    $post->setPostId($_POST[('edit_post_id')]);
    $post->setPostTitle($_POST[('post_title')]);
    $post->setPostByUser($_POST[('post_user')]);
    $post->setPostDescription($_POST[('post_description')]);
    $post->editPost();

}


if (isset($_GET[('id')]))
{

    $post->setPostId($_GET[('id')]);
    $post->deletePost();

}