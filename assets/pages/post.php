<?php

    include "../includes/header.php";
    include "BlogUser.php";
    include "../process/commentsFormProcessing.php";
    include "BlogPost.php";
    include "like.php";

    //include "../process/Comments.php";
    $postObject = new BlogPost();
    $comment = new Comments();

/* *   $comments = new Comments();
 *   Note i have to include this file but when i display it in browser it gives me an error that
 *   databsase name is already in use some other stuff like that.
 *   after debugging i came to know that i create Comments class object as commentsObject in
 *   commentsFromProcessing file. so keep it in mind for now and for future too.
 */

?>
    <!-- navbar for posts page begin -->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">BLOG</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- navbar for posts page end -->

    <!-- Modal for Posts begin-->

    <div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create A Post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                if (isset($_POST['read_more'])) {
                    $post = $_POST['postid'];

                    $postObject->setPostId($post);
                    $editRow = $postObject->fetchParticularPost();

                    //  print_r($row);
                }
                ?>
                <div class="modal-body">
                    <form method="post" id="post-form">
                        <div class="row">
                            <div class="form-group col-sm-6">

                                <label class="text-inverse" for="post_title">Post Title</label>
                                <input type="text" class="form-control" id="post-title" placeholder="A Great World" name="post_title" value="<?php echo $editRow[0]['post_title']; ?>">
                                <p id="post-title-error"></p>
                            </div>
                            <input type="hidden" name="edit_post_id" value="<?php echo $editRow[0]['post_id']; ?>">
                            <div class="form-group col-sm-6 ml-auto">
                                <label for="post_user">Select User:</label>
                                <select class="form-control" id="post-user" name="post_user">
                                    <option value="">Please Select User</option>

                                    <!-- to display users in the drop down -->

                                    <?php
                                    $user= new User();
                                    $myrows=$user->display_user();

                                    if($myrows!=0)
                                    {

                                        foreach ($myrows as $row)
                                        {

                                            ?>
                                            <option value="<?php echo $row["user_name"]; ?>"><?php echo $row["user_name"]; ?></option>
                                            <?php
                                        }

                                    }
                                    else
                                    {
                                        echo"no users yet!";
                                    }
                                    ?>

                                </select>
                                <p id="post-user-error"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-inverse" for="post_descriptioon">POST Description</label>
                            <textarea class="form-control" rows="5" id="post-descriptioon" name="post_description" placeholder="Description"><?php echo $editRow[0]['post_description']; ?></textarea>
                            <p id="post-description-error"></p>
                        </div>
                        <div class="modal-footer col-sm-12">
                            <button type="submit" class="btn btn-outline-dark" id="update-post" name="update_post">Update Post</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Posts end-->

<!-- Blog Posts Begin -->
<div class="container mt-5">
    <h2 class="my-4 text-center bg-dark text-light">BLOGS Are Awesome</h2>
    <div class="row">
        <div class="col-sm-8">
            <!-- Blog Post -->
            <div class="card mb-4">

            <?php
            if (isset($_POST['read_more'])) {
                $post = $_POST['postid'];
                $postObject->setPostId($post);
                $row = $postObject->fetchParticularPost();
                //  print_r($row);

                ?>

                <div class="card-body">

                <h2 class="card-title"><?php echo $row[0]['post_title']; ?></h2>
                <p class="card-text"><?php echo $row[0]['post_description']; ?></p>
                <a class="btn btn-outline-success" data-toggle="modal" data-target="#postModal" name="edit_post">Edit <i class="fa fa-pencil-square"></i></a>
<!--                <a href="" class="btn btn-outline-danger">Delete <i class="fa fa-trash"></i></a>-->
                <a href='post.php?id=<?php echo $row[0]["post_id"]; ?>' onclick='return checkdelete()' class="btn btn-outline-danger" name="delete">Delete <i class="fa fa-trash"></i></a>

                <!--<button  class="btn btn-outline-warning">disLikes <i class="fa fa-thumbs-down" aria-hidden="true"></i></button>-->
                    <a id="" class="btn btn-outline-info js-rating" data-like="8"></a>
                    <div class="card-footer text-muted mt-3 ">
                    <i class="fa fa-clock-o"></i> Posted on <span class="text-primary"><?php echo $row[0]['post_time']; ?></span> by
                    <a class="text-warning"><?php echo $row[0]['post_by_user']; ?></a>
                </div>
            </div>
                <?php
            } else
            {
                echo "<h3 class='text-secondary text-center '>Please Select a post first</h3>";
            }

            ?>


        </div>
            <?php
            if (isset($_POST['read_more'])) {

                ?>
            <!-- Comments Form -->
            <div class="card card-footer">
                <h4>Leave a Comment:</h4>
                <p class="error text-danger"></p>
                <form action="" method="post" id="comment-form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="postID" value="<?php echo $row[0]['post_id']; ?>">
                        <textarea class="form-control" rows="3" id="comment-description" name="comment_description"></textarea>
                        <p id="comment-description-error"></p>
                    </div>
                    <button type="submit" class="btn btn-outline-dark" name="submit_comment">Submit</button>
                </form>
            </div>

            <?php
            // $commentObject is comming from the commentsFormProcessing .php (LOGIC)
                $comment->setPostId($row[0]['post_id']);
            $myRows = $commentsObject->fetchComments($row[0]['post_id']);
            if ($myRows != 0)
            {
                foreach ($myRows as $rows) {
               ?>
                    <!-- Comment -->
                    <div class="comments pt-2" id="comments">
                        <div class="float-left mr-3">
                            <img class="" src="http://placehold.it/64x64" alt="">
                        </div>
                        <div class="">
                            <h5 class="text-warning"><?php echo $rows["commented_user"]; ?>
                                <small class="text-secondary">Commented at</small>
                                <i class="fa fa-clock-o text-secondary"></i>
                                <small class="text-primary"><?php echo $rows["commented_time"]; ?></small>
                            </h5>
                                <form action="" method="post" class="d-inline">
<!--                                    <a href="post.php?id=--><?php //echo $rows["comment_id"]; ?><!--" data-toggle="modal" data-target="#comment-modal" class="btn btn-outline-warning float-right"  id="edit-comment" name="edit_comment" role="button" type="submit"><i class="fa fa-pencil"></i></a>-->
                                    <input type="hidden" name="comment_id" value="<?=$rows['comment_id']?>">
                                    <button type="submit" name="delete_comment" class="btn btn-outline-danger float-right" onclick="return confirm('are you sure you want to delete it')" ><i class="fa fa-trash"></i></button>
                                </form>
                            <p><?php echo $rows["comment_description"]; ?></p>
                        </div>
                    </div>
                    <?php
                }
            }
            else
            {
                echo "<h3 class='text-secondary text-center'>No Comments yet</h3>";
            }
            }
            ?>

        </div>

        <!-- side widget column -->
        <div class="col-sm-4">


            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Side Widget</h5>
                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, molestiae neque. Eveniet iusto magnam minima, mollitia odio repellendus. Assumenda beatae consequuntur corporis cum doloribus in obcaecati, officiis repudiandae vel voluptates.
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Modal for editing comments-->
    <div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="post" id="comment-edit-form">
                        <div class="row">
                            <div class="form-group col-sm-12">

                                <label class="text-inverse" for="comment_edit_description">Comment Description</label>
                                <textarea class="form-control" rows="5" id="comment-edit-description" name="comment_edit_description" placeholder="Description"><?php ?></textarea>
                                <p id="comment-edit-description-error"></p>
                            </div>
                            <div class="modal-footer col-sm-12">
                                <input type="hidden" name="comment_id" value="<?php ?>">
                                <button type="submit" class="btn btn-outline-dark" id="update-comment" name="update_comment">UPDATE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for users end-->

<?php include "../includes/footer.php" ?>
<script>

    function checkdelete()
    {
        return confirm("Are you sure you want to delete this record?");
    }
</script>
