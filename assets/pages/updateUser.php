<?php
include "../includes/header.php";
//include "BlogUser.php";
//include "users.php";

?>
<!-- navbar for posts page begin -->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">BLOG</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
        </ul>
    </div>
</nav>

<!-- navbar for posts page end -->

<!--body starts from here-->
<html>
<body>

<div class="bg">
    <div class="user-data">
        <form class="form-horizontal" action="BlogUser.php  " method="get">
            <div class="form-group">
                <input type="hidden" class="form-control"   name="user_id" value="<?php echo $_GET['rn'];?>">
                <label class="control-label col-sm-2" for="email">First Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="user-name"  placeholder="First Name" name="user_name" value="<?php echo $_GET['fn'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Email:</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="user-email" placeholder="Email" name="user_email" value="<?php echo $_GET['email'];?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default" name="update" id="update-user">UPDATE</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>


<!--body ends here-->

<?php
include "../includes/footer.php";
?>

