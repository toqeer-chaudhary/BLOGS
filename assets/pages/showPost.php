<?php
    $myrow = $postObject->fetchPosts();
    foreach ($myrow as $row) {
        ?>
        <div class="card mb-4">
            <div class="card-body">
                <h2 class="card-title"><?php echo $row['post_title']; ?></h2>
                <p class="card-text"><?php echo $row['post_description']; ?></p>
                <form action="post.php" method="POST">
                    <input type="hidden" name="postid" value="<?php echo $row['post_id']; ?>">
                    <input type="submit" class="btn btn-outline-dark" name="read_more" value="Read More &rarr;">
                </form>
            </div>
            <div class="card-footer text-muted">
                <i class="fa fa-clock-o"></i> Posted on <span class="text-primary"><?php echo $row['post_time']; ?></span> by
                <a class="text-warning"><?php echo $row['post_by_user']; ?></a>
            </div>
        </div>
        <?php
    }
?>