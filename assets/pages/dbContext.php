<?php

    // The purpose of this file is to read data from .env file and
    // Create generalize functions for database manipulation.

    // reading data from .env file

    $myfile       = fopen("../../.env", "r") or die("Unable to open file!");
    $file         =  fread($myfile,filesize("../../.env"));
    fclose($myfile);

    // doing some manipulation with data to get desired results.

    $separation_1 = explode("\n",$file);
    $joining_1    = implode("=",$separation_1);
    $separation_2  = explode("=",$joining_1);

    // declaring variables constant so that they can be used in dbContext class

    define("HOST",trim($separation_2[1]));
    define("USERNAME",trim($separation_2[3]));
    define("PASSWORD",trim($separation_2[5]));
    define("DATABASE",trim($separation_2[7]));

    // creating class which will provide general methods for CRUD.
class DbContext
{
    private $host;
    private $username;
    private $password;
    private $database;
    private $connection;

    public function __construct()
    {
        // initializing class variables with constants
        $this->host = HOST;
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $this->database = DATABASE;

        //call open function
        $this->open_connection();
    }

    //Open function
    public function open_connection()
    {
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);

        if ($this->connection->connect_error) {
            die('SORRY we are down right now!!!' . mysqli_connect_error());
        } else {
            echo "";
        }
    }

    //insert
    public function insert($query)
    {
        //echo $query;
        $insertRow = $this->connection->query($query);
        if ($insertRow) {

            //echo "Data inserted successfully";
            // to be discussed this header part.
            //  header("Location: " );

            // to be discussed this part.
            header('Location: ' . $_SERVER['HTTP_REFERER'] );

            exit();
        } else {

            die("Data is not inserted successfully");


        }
    }

    //update
    public function update($query)
    {
        echo "$query";
        $updateRow = $this->connection->query($query);
        if ($updateRow) {
            echo "Data edited successfully";
            // ("Location: ");
          //  header('Location: ' . $_SERVER['HTTP_REFERER'] );
            header('Location: ' . $_SERVER['HTTP_REFERER'] );
            exit();
        } else {
            die("Data is not Updated successfully");
        }
    }

    //Delete
    public function delete($query)
    {
        $deleteRow = $this->connection->query($query);
        if ($deleteRow) {
           // echo "Row deleted successfully";
            // ("Location: ");
            // exit();
            header('Location: ' . $_SERVER['HTTP_REFERER'] );
        } else {
            die("Row is not Deleted successfully");
        }
    }


    //Select
    public function select($query)
    {

        $sql = mysqli_query($this->connection,$query);
        $numberOfRows = $sql->num_rows;
        $rows = array();

        if ($numberOfRows > 0)
        {
            while ($row = mysqli_fetch_assoc($sql))
            {
                $rows[] = $row;
            }
            return $rows;
        }
        else
        {
            return $rows;
        }


    }


    //Close function
    public function close_connection()
    {
        if (isset($this->connection)) {
            mysqli_close($this->connection);
            unset($this->connection);
        }
    }
}

//$db = new DbContext();
//print_r($db->select("SELECT * FROM `posts` WHERE post_id = 4"));
//print_r($db->select("SELECT * FROM comments"));


