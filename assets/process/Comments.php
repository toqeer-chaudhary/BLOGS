<?php
   // include "../pages/dbContext.php";

class Comments {

    private $sql;
    private $db;
    private $commentId;
    private $commentDescription;
    private $foriegnKey;
    private $postId;

    /**
     * @return mixed
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * @param mixed $postId
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
    }
    /**
     * @return mixed
     */
    public function getForiegnKey()
    {
        return $this->foriegnKey;
    }

    /**
     * @param mixed $foriegnKey
     */
    public function setForiegnKey($foriegnKey)
    {
        $this->foriegnKey = $foriegnKey;
    }

    public function __construct(){
        $this->db = new DbContext();
    }

    // get the comment description
    public function getCommentDescription()
    {
        return $this->commentDescription;
    }

    // set the comment Descritption
    public function setCommentDescription($commentDescription)
    {
        $this->commentDescription = $commentDescription;
    }

    // get the comment Id
    public function getCommentId()
    {
        return $this->commentId;
    }

    // set the comment Id
    public function setCommentId($commentId)
    {
        $this->commentId = $commentId;
    }


    // insert comment
    public function insertComment()
    {
        $this->sql = "INSERT INTO `comments` (`comment_id`, `comment_description`, `commented_user`, `commented_time`, `post_fk`) 
                        VALUES (NULL, '".$this->getCommentDescription()."', 'Toqeer', CURRENT_TIMESTAMP, '".$this->getForiegnKey()."');";
        $this->db->insert($this->sql);

    }

    // delete comment
    public function deleteComment()
    {
        // echo $this->comment_id;
        $this->sql = "DELETE FROM `comments` WHERE comment_id = '".$this->getCommentId()."'";
        $this->db->delete($this->sql);
    }

    public function editComment()
    {
        $this->sql = "UPDATE `comments` SET `comment_description` = '".$this->getCommentDescription()."' WHERE `comment_id` = '".$this->getCommentId()."'";
        $this->db->update($this->sql);
    }

    public function fetchComments($id)
    {

        $this->sql = "SELECT * FROM `comments` JOIN `posts` ON posts.post_id = comments.post_fk WHERE comments.post_fk =$id ";

        $check = $this->db->select($this->sql);

        if ($check != 0)
        {
            return $check;
        }
        else
        {
            return $check;
        }
    }

    public function fetchParticularComment()
    {
        $this->sql = "SELECT * FROM `comments` WHERE `comment_id` = '".$this->getCommentId()."'";
        $check = $this->db->select($this->sql);
        return $check;
    }
}

//$comments = new Comments();
//print_r($comments->fetchParticularComment());