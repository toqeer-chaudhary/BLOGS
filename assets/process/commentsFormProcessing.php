<?php

    // this file will process the form for comments i.e.
    # Creating comment
    # Editiing comment
    # Deleting comment

    // including comments file and creating its object.
    include "Comments.php";
    $commentsObject = new Comments();

    // this condition will work if user add a comment
    if (isset($_POST["submit_comment"]))
    {
        if (!(empty($_POST["comment_description"])))
        {
            $postId =  $_POST['postID'];
            $commentsDescription = $_POST['comment_description'];
            $commentsObject->setForiegnKey($postId);
            $commentsObject->setCommentDescription($commentsDescription);
            $commentsObject->insertComment();
        }
    }

    // this condition will work if user delete comment.
    if (isset($_POST["delete_comment"]))
    {
        if (isset($_POST["comment_id"]))
        {
            $commentId = $_POST["comment_id"];
            $commentsObject->setCommentId($commentId);
            $commentsObject->deleteComment();
        }
    }



//    if (isset($_GET["id"]))
//    {
//        $commentId = $_GET["id"];
//        $commentsObject->setCommentId($commentId);
//        $fetch = $commentsObject->fetchParticularComment();
//        echo $fetch[0]["comment_id"];
//    }



// this condition will work if user edit comment.
    if (isset($_POST["update_comment"]))
    {
        if ((isset($_POST["comment_id"])) && !empty($_POST["comment_edit_description"]))
        {
            $commentId = $_POST["comment_id"];
            $commentsDescription = $_POST["comment_edit_description"];

            $commentsObject->setCommentId($commentId);
            $commentsObject->setCommentDescription($commentsDescription);
            $commentsObject->editComment();
        }
    }

